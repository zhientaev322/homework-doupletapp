from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin

from .models import User

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    field = ("name","email")
    list_display = ("id","name","email")