import logging

from django.core.management.base import BaseCommand
from django.conf import settings
from telegram import Bot, ReplyKeyboardRemove, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from telegram.ext import CallbackQueryHandler
from telegram.utils.request import Request
import requests
import re
import environ
from app.models import User

regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')

NAME, EMAIL, AGE, PHONE = range(4)
user_data = {}
env = environ.Env()

def check_email(email):
    return re.fullmatch(regex, email)


def check_age(age):
    try:
        int(age)
        if (int(age) < 1):
            return False
        return True
    except ValueError:
        return False


def check_phone(phone):
    return re.match(r'^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$', phone)


def start_handler(update: Update, context: CallbackContext):
    name_bot = update.message.bot.name
    update.message.reply_text(
        text=f'Вас приветствует {name_bot}\n\n' +
             'Введите имя пользователя, чтобы продолжить',
        reply_markup=ReplyKeyboardRemove(),
    )
    return NAME


def name_handler(update: Update, context: CallbackContext):
    user_data[NAME] = update.message.text

    update.message.reply_text(
        f'Введите электронную почту:'
    )
    return EMAIL


def email_handler(update: Update, context: CallbackContext):
    email = update.message.text
    if (check_email(email) is None):
        update.message.reply_text('Укажите корректную почту')
        return EMAIL
    user_data[EMAIL] = email

    update.message.reply_text(
        f'Введите свой возраст:'
    )
    return AGE


def age_handler(update: Update, context: CallbackContext):
    age = update.message.text
    if check_age(age) == False:
        update.message.reply_text("Введите корректное значение!")
        return AGE
    user_data[AGE] = age

    update.message.reply_text(
        f'Введите номер телефона:'
    )
    return PHONE


def phone_handler(update: Update,context: CallbackContext):
    phone = update.message.text
    if (check_phone(phone)==False):
        update.message.reply_text("Введите корректный номер")
        return PHONE
    user_data[PHONE] = phone
    try:
        user_check = User.objects.get(id = update.message.from_user.id)
        update.message.reply_text(
            text = "Пользователь с таким id уже существует, вы хотите перезаписать данные?",
            reply_markup=markup,
        )
    except:
        user = User(id=update.message.from_user.id, name=user_data[NAME], email=user_data[EMAIL], age=user_data[AGE],
                    phone=user_data[PHONE])
        user.save()
        user_data.clear()
        update.message.reply_text(
            f'Данные успешно сохранены'
        )
    return ConversationHandler.END

def cancel_handler(update: Update, context: CallbackContext):
    update.message.reply_text("Вы отменили ввод анкеты. Для того, чтобы ввести анкету заного, введите команду /start")

def set_phone_handler(update: Update,context: CallbackContext):
    phone = " ".join(context.args)
    if (check_phone(phone) == False ):
        update.message.reply_text("Введите корректный номер телефона!")
    try:
        user = User.objects.get(id = update.message.from_user.id)
        update.message.reply_text("Для начала заполните все данные с помощью команды /start")
    except:
        user.phone = phone
        user.save()
        update.message.reply_text("Данные успешно изменены")


def any_handler(update: Update, context: CallbackContext):
    URL = 'https://api.telegram.org/bot'
    TOKEN = env("TELEGRAM_TOKEN")
    chat_id = update.message.from_user.id
    if update.message.text == "GET /me":
        try:
            user =User.objects.get(id = update.message.from_user.id)
            text = "Профиль пользователя:\n\n" + "Имя пользователя: " + user.name + "\n Почта: " + user.email + "\n Возраст: " + str( user.age) + "\n Телефон: " + str(user.phone)
            requests.get(f'{URL}{TOKEN}/sendMessage?chat_id={chat_id}&text={text}')
        except:
            update.message.reply_text(
                text = f'Что-то произошло не так...'
            )
    else:
        update.message.reply_text(
            text=f'Введите корректную команду!',
        )
    return ConversationHandler.END

def me_handler(update: Update,context:CallbackContext):
    try:
        user = User.objects.get(id = update.message.from_user.id)
        update.message.reply_text(
            text = "Профиль пользователя:\n\n" +
                   "Имя пользователя: " + user.name + "\n Почта: " +user.email +"\n Возраст: " + str(user.age) + "\n Телефон: " + str(user.phone),
        )
    except:
        update.message.reply_text("Для начала заполните анкету, воспользовавшись командой /start")

get_keyboard = [
    [
        InlineKeyboardButton("Да",callback_data='yes'),
        InlineKeyboardButton("Нет",callback_data='no'),
    ],
]

markup = InlineKeyboardMarkup(get_keyboard)

def button(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    id = query.from_user.id
    query.answer()
    if (query.data == 'yes'):
        user = User(id=id, name=user_data[NAME], email=user_data[EMAIL], age=user_data[AGE],
                    phone=user_data[PHONE])
        user.save()
        query.message.reply_text(
            f'Данные успешно сохранены'
        )
        user_data.clear()
    else:
        query.edit_message_text(text= "Вы отменили заполнение профиля")


class Command(BaseCommand):
    help = "Телеграм-бот"

    def handle(self, *args, **options):
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0,
        )
        bot = Bot(
            request=request,
            token=settings.TOKEN,
        )
        print(bot.get_me())

        updater = Updater(
            bot=bot,
            use_context=True,
        )

        conv_handler = ConversationHandler(
            entry_points=[
                CommandHandler('start', start_handler),
            ],
            states={
                NAME: [
                    MessageHandler(Filters.text & (~ Filters.command), name_handler, pass_user_data=True),
                ],
                EMAIL: [
                    MessageHandler(Filters.text & (~ Filters.command), email_handler, pass_user_data=True),
                ],
                AGE: [
                    MessageHandler(Filters.text & (~ Filters.command), age_handler, pass_user_data=True),
                ],
                PHONE:[
                    MessageHandler(Filters.text & (~ Filters.command),phone_handler,pass_user_data=True),
                ]
            },
            fallbacks=[
                CommandHandler('cancel', cancel_handler)
            ],
        )
        updater.dispatcher.add_handler(conv_handler)
        command_handler_start = CommandHandler('start',start_handler)
        updater.dispatcher.add_handler(command_handler_start)
        command_handler_phone = CommandHandler('set_phone', set_phone_handler)
        updater.dispatcher.add_handler(command_handler_phone)

        command_handler_me = CommandHandler('me',me_handler)
        updater.dispatcher.add_handler(command_handler_me)
        updater.dispatcher.add_handler(CallbackQueryHandler(button))
        message_handler = MessageHandler(Filters.text, any_handler)
        updater.dispatcher.add_handler(message_handler)

        updater.start_polling()
        updater.idle()
