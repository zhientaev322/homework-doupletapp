from django.contrib.auth.models import AbstractUser
from django.db import models

class AdminUser(AbstractUser):
    username = models.CharField(max_length=25,unique=True)
    email = models.EmailField(unique=True)
    def __str__(self):
        return f'{self.username}'

    class Meta:
        verbose_name = 'Профиль админа'
        verbose_name_plural = 'Администраторы'
