import uuid

from app.internal.models.admin_user import AdminUser
from django.db import models


class User(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    age = models.PositiveIntegerField(default = 0)
    phone = models.CharField(max_length=12)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Пользователи'
